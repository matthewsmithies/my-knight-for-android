package com.falcon.protect.dialog;

import com.falcon.font.AppFont;
import com.falcon.intent.Email;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.myknight.R;
import com.falcon.regex.RegexComponent;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ExportReport extends Dialog {

	private String report;

	private ProtectActivity activity;

	public ExportReport(ProtectActivity activity, String report) {
		super(activity.getContext());
		this.report = report;
		this.activity = activity;
		initiliaseExportDialog();
	}

	private void initiliaseExportDialog() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.export_strategy);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView title = (TextView) findViewById(R.id.exportText);
		EditText email = (EditText) findViewById(R.id.exportEmail);
		Button send = (Button) findViewById(R.id.exportButton);

		AppFont appFont = new AppFont(getContext());

		title.setTypeface(appFont.getBasicFont());
		email.setTypeface(appFont.getBasicFont());
		send.setTypeface(appFont.getBasicFont());

		send.setOnTouchListener(buttonTouch(send));
		send.setOnClickListener(exportListener(email));

	}

	private View.OnClickListener exportListener(final EditText edit) {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (RegexComponent.isEmailValid(edit)) {
					Email email = new Email(edit.getText().toString().trim(),
							getReport());
					
					email.sendEmail(getActivity().getActivity());
				} else {
					animateTitle(getActivity().getString(R.string.email_error),
							R.drawable.msg_border_error);
				}
			}
		};
	}

	private void animateTitle(String string, int resId) {
		Animation anim = AnimationUtils.loadAnimation(getContext(),
				R.anim.animation_text);

		AppFont appFont = new AppFont(getContext());

		TextView title = (TextView) findViewById(R.id.exportText);
		TextView dialogMsg = (TextView) findViewById(R.id.strategyMessage);

		dialogMsg.setTypeface(appFont.getBasicFont());

		title.setVisibility(View.INVISIBLE);

		dialogMsg.setVisibility(View.VISIBLE);
		dialogMsg.setBackgroundResource(resId);
		dialogMsg.setText(string);
		dialogMsg.startAnimation(anim);
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private ProtectActivity getActivity() {
		return activity;
	}

	private String getReport() {
		return report;
	}
}
