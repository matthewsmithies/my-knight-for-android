package com.falcon.protect.dialog;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.AccountActivity;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.Breach;
import com.falcon.protect.model.User;
import com.falcon.myknight.R;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmFix extends Dialog {

	private Breach breach;
	private int breachPosition;
	private int accountPosition;
	private ProtectActivity protectActivity;

	public ConfirmFix(ProtectActivity activity, Breach breach,
			int breachPosition, int accountPos) {
		super(activity.getContext());
		this.accountPosition = accountPos;
		this.breachPosition = breachPosition;
		this.breach = breach;
		this.protectActivity = activity;
		initialiseConfirmDialog();
	}

	private void initialiseConfirmDialog() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.confirm_fix);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		AppFont font = new AppFont(getContext());

		TextView title = (TextView) findViewById(R.id.confirmTitle);
		Button yes = (Button) findViewById(R.id.confirmYes);
		Button no = (Button) findViewById(R.id.confirmNo);

		title.setTypeface(font.getBasicFont());
		yes.setTypeface(font.getBasicFont());
		no.setTypeface(font.getBasicFont());

		yes.setOnTouchListener(buttonTouch(yes));
		no.setOnTouchListener(buttonTouch(no));

		// change the data model
		yes.setOnClickListener(fixBreach());
		no.setOnClickListener(buttonClick());

		title.setText(getDialogInfo());
	}

	private View.OnClickListener fixBreach() {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				changeAccountStatus(getAccountPosition(), getBreachPosition());
				dismiss();
				accountActivity();
			}

			private void accountActivity() {
				getProtectActivity().finish();
				Intent intent = new Intent(getContext(), AccountActivity.class);
				intent.putExtra(Account.ACC_POS, getAccountPosition());
				getContext().startActivity(intent);
			}
		};
	}

	private View.OnClickListener buttonClick() {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		};
	}

	public void changeAccountStatus(int accountPosition, int breachPosition) {
		User user = new User(getContext());
		user.getAccounts().get(accountPosition).setBreachStatus(breachPosition);
		user.exportUserData();
	}

	// extend
	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private String getDialogInfo() {
		return getContext().getText(R.string.confirm_fix) + " "
				+ getBreach().getDomain() + "?";
	}

	private Breach getBreach() {
		return breach;
	}

	public int getBreachPosition() {
		return breachPosition;
	}

	public int getAccountPosition() {
		return accountPosition;
	}

	public ProtectActivity getProtectActivity() {
		return protectActivity;
	}

	public void setProtectActivity(ProtectActivity protectActivity) {
		this.protectActivity = protectActivity;
	}
}
