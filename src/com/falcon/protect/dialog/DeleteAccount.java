package com.falcon.protect.dialog;

import java.util.List;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.model.Account;
import com.falcon.myknight.R;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DeleteAccount extends Dialog {

	private ProtectActivity protectActivity;
	private List<Account> accounts;
	private int position;

	public DeleteAccount(ProtectActivity protectActivity, int position,
			List<Account> accounts) {
		super(protectActivity.getContext());
		this.protectActivity = protectActivity;
		this.position = position;
		this.accounts = accounts;
		initialiseDeleteDialog();
	}

	private void initialiseDeleteDialog() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.delete_email);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		AppFont font = new AppFont(getContext());

		Button yesDelete = (Button) findViewById(R.id.yesDelete);
		Button noDelete = (Button) findViewById(R.id.noDelete);
		TextView deleteTitle = (TextView) findViewById(R.id.deleteTitle);

		yesDelete.setTypeface(font.getBasicFont());
		noDelete.setTypeface(font.getBasicFont());
		deleteTitle.setTypeface(font.getBasicFont());

		noDelete.setOnTouchListener(buttonTouch(noDelete));
		yesDelete.setOnTouchListener(buttonTouch(yesDelete));

		noDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				ProtectActivity.setPressTrue();
			}
		});

		yesDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().delete(getAccounts(), getPosition());
				ProtectActivity.setPressTrue();
				dismiss();
			}
		});
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private ProtectActivity getActivity() {
		return protectActivity;
	}

	public ProtectActivity getProtectActivity() {
		return protectActivity;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public int getPosition() {
		return position;
	}

}
