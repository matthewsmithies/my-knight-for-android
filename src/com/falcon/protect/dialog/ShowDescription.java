package com.falcon.protect.dialog;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.model.Breach;
import com.falcon.myknight.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ShowDescription extends Dialog {

	private Breach breach;

	public ShowDescription(Context context, Breach breach) {
		super(context);
		this.breach = breach;
		initDialog();
	}

	private void initDialog() {

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.show_description);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView title = (TextView) findViewById(R.id.showTitle);
		TextView description = (TextView) findViewById(R.id.showDescription);
		Button okButton = (Button) findViewById(R.id.showOkButton);

		AppFont appFont = new AppFont(getContext());
		title.setTypeface(appFont.getBoldFont());
		description.setTypeface(appFont.getBasicFont());
		okButton.setTypeface(appFont.getBasicFont());

		title.setText(getBreach().getTitle());

		String descriptionText = Html.fromHtml(getBreach().getDescription())
				.toString();
		description.setText(descriptionText);

		okButton.setOnTouchListener(buttonTouch(okButton));

		okButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	public Breach getBreach() {
		return breach;
	}

}
