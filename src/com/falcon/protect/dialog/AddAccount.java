package com.falcon.protect.dialog;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.User;
import com.falcon.protect.service.GetApiEmail;
import com.falcon.protect.service.ResponseJsonListener;
import com.falcon.myknight.R;
import com.falcon.regex.RegexComponent;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AddAccount extends Dialog {

	private Button button;
	private TextView textView;
	private TextView dialogMsg;
	private EditText editText;

	private RelativeLayout border;

	private ProtectActivity protectActivity;
	private ResponseJsonListener responseJsonListener;

	public AddAccount(ProtectActivity protectActivity,
			ResponseJsonListener responseJsonListener) {
		super(protectActivity.getContext());
		this.protectActivity = protectActivity;
		this.responseJsonListener = responseJsonListener;
		initialiseDialog();
	}

	private void initialiseDialog() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.add_email);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		AppFont font = new AppFont(getContext());

		border = (RelativeLayout) findViewById(R.id.DialogBorder);
		border.setBackgroundResource(R.drawable.dialog_border);

		dialogMsg = (TextView) findViewById(R.id.dialogMessage);
		textView = (TextView) findViewById(R.id.addTitle);
		button = (Button) findViewById(R.id.exportButton);
		editText = (EditText) findViewById(R.id.exportEmail);

		dialogMsg.setTypeface(font.getBasicFont());
		editText.setTypeface(font.getBasicFont());
		button.setTypeface(font.getBasicFont());
		textView.setTypeface(font.getBasicFont());

		button.setOnTouchListener(buttonTouch());
		button.setOnClickListener(buttonClick());
	}

	private View.OnClickListener buttonClick() {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!ProtectApplication.isInterwebsOn(getContext())) {
					protectActivity.getActionBar().setTitle("network not available");
					dismiss();
					return;
				}
				
				if (RegexComponent.isEmailValid(editText)) {
					 animateTitle(
					 protectActivity.getString(R.string.email_twice),
					 R.drawable.msg_border_pass);
					if (emailExists(editText.getText().toString())) {
						checkEmailPwnage(editText.getText().toString());
						dismiss();
					}
				} else {
					animateTitle(
							protectActivity.getString(R.string.email_error),
							R.drawable.msg_border_error);
				}
			}

			private boolean emailExists(String email) {

				//return true if no data exists
				if (!getActivity().hasSavedData())
					return true;

				User user = new User(getContext());
				
				for (int i = 0; i < user.getAccounts().size(); i++) {
					Account account = user.getAccounts().get(i);
					if (account.getEmail().matches(email)) {
						return false;
					}
				}
				return true;
			}
		};
	}

	//extend
	private View.OnTouchListener buttonTouch() {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private void animateTitle(String string, int resId) {
		Animation anim = AnimationUtils.loadAnimation(getContext(),
				R.anim.animation_text);

		textView.setVisibility(View.INVISIBLE);
		dialogMsg.setVisibility(View.VISIBLE);
		dialogMsg.setBackgroundResource(resId);
		dialogMsg.setText(string);
		dialogMsg.startAnimation(anim);
	}

	// handle to make full offline!
	private void checkEmailPwnage(String email) {

		// offline loading. Not perfect.
		processingEmail(email);

		if (ProtectApplication.isInterwebsOn(getContext())) {
			getActivity().setProgressBarIndeterminateVisibility(true);
			GetApiEmail apiEmail = new GetApiEmail(getResponseJsonListener());
			apiEmail.execute(email);
		} else {
			Log.e("INTERWEBS-ERROR", "NO INTERWEBS:-(");
		}
	}

	public ResponseJsonListener getResponseJsonListener() {
		return responseJsonListener;
	}

	private ProtectActivity getActivity() {
		return protectActivity;
	}

	private void processingEmail(String email) {
		if (getActivity().hasSavedData()) {
			User user = new User(getContext());
			Account loadingAccount = new Account(email);
			user.addAccount(loadingAccount);
			getActivity().setGridAdapter(user.getAccounts());
		}

		getActivity().getActionBar().setTitle("Adding Account...");
	}

}
