package com.falcon.protect.dialog;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.User;
import com.falcon.protect.service.GetApiEmail;
import com.falcon.protect.service.ResponseJsonListener;
import com.falcon.myknight.R;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class NoBreach extends Dialog {

	private ProtectActivity protectActivity;
	private Account account;
	private ResponseJsonListener listener;
	private int position;

	public NoBreach(ProtectActivity protectActivity, Account account,
			ResponseJsonListener responseJsonListener, int position) {
		super(protectActivity.getContext());
		this.protectActivity = protectActivity;
		this.account = account;
		this.listener = responseJsonListener;
		this.position = position;
		initialiseDialog();
	}

	private void initialiseDialog() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.not_breached_account);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		AppFont font = new AppFont(getContext());

		Button refresh = (Button) findViewById(R.id.refreshButton);
		TextView title = (TextView) findViewById(R.id.refreshTitle);

		refresh.setTypeface(font.getBasicFont());
		title.setTypeface(font.getBasicFont());

		refresh.setOnTouchListener(buttonTouch(refresh));

		refresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getProtectActivity()
						.setProgressBarIndeterminateVisibility(true);
				GetApiEmail apiEmail = new GetApiEmail(getListener());
				apiEmail.execute(account.getEmail());
				User user = new User(getContext());
				user.getAccounts().remove(position);
				user.exportUserData();
				getProtectActivity().getAccountAdapter().remove(account);
				loadingDialog(getAccount());
				dismiss();
			}
		});
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private void loadingDialog(Account account) {
		if (getProtectActivity().hasSavedData()) {
			User user = new User(getContext());
			Account loadingAccount = new Account(account.getEmail());
			user.addAccount(loadingAccount);
			getProtectActivity().setGridAdapter(user.getAccounts());
//			setContentView(getProtectActivity().setMainView());
		}

		getProtectActivity().getActionBar().setTitle("Adding Account...");
	}

	public ProtectActivity getProtectActivity() {
		return protectActivity;
	}

	public Account getAccount() {
		return account;
	}

	public ResponseJsonListener getListener() {
		return listener;
	}

	public int getPosition() {
		return position;
	}

}
