package com.falcon.protect.lexical;

import java.util.List;
//add water mark

import com.falcon.protect.activities.ProtectApplication;

//probably will change in future, add new breaches
public class UserStrategy {

	private final String EMAIL_ADDRESSES = "Email addresses";
	private final String PASSWORD_HINTS = "Password hints";
	private final String PASSWORDS = "Passwords";
	private final String USERNAMES = "Usernames";
	private final String CREDIT_CARDS = "Credit cards";
	// public final String GENDERS = "Genders";
	// public final String DOB = "Dates of birth";
	private final String GEO = "Geographic location";
	private final String HIST_PASS = "Historical passwords";
	private final String IM_IDS = "Instant messenger identities";
	// public final String IP_ADDRS = "IP addresses";
	// private final String PM = "Private messages";
	// public final String USER_WEB = "User website URLs";
	// public final String NICKNAME = "Nicknames";
	// public final String YEARS_BIRTH = "Years of birth";
	private final String ADDRS = "Addresses";
	// public final String AGE_GROUPS = "Age groups";
	// public final String EMPLOYERS = "Employers";
	// public final String NAMES = "Names";
	private final String PHONE_NOS = "Phone numbers";
	private final String WEB_ACTIVITY = "Website activity"; // patterns
	// public final String JOB_TITLES = "Job titles";
	private final String REWARD_BALANCES = "Reward program balances"; // patterns
	private final String GOVERNMENT_IDS = "Government issued IDs";
	private final String PURCHASES = "Purchases"; // patterns
	// public final String SMS = "SMS messages";

	// domain
	private final String DOMAIN_EMAIL = "Make sure to change your personal information such as: passwords, hints and secret answers on ";
	private final String DOMAIN_STRING = " to help secure your account";

	// descriptions
	private final String EMAIL_DESC = "Your email can be targeted to send spam, make sure to you have a spam filter in place. If your passwords are at risk then your email may be used to send malicious content to your contacts. ";
	private final String HINTS_DEC = "Easy password hints can lead directly to your real password, remember to use an obsure hint that you understand but cannot be easily extracted. ";
	private final String PASSWORDS_DESC = "Passwords are usually are the biggest cause for concern for internet security. Remember to use a unique password that you can remember. A great trick is to use 4-5 words that are easy for you to remember but random. Also it is more then possible for a breached encrypted password to be found if a common password was first used. Make sure that you have an different passwords for different personal accounts as in addition to your email address malicious individuals may have access. ";
	private final String USERNAME_DESC = "Although usernames are common knowledge, your accounts can be breached in addition to a breached password. As a whole real users try to have the same usernames over all of their accounts. ";
	private final String CREDIT_DESC = "Make sure to contact your bank as soon as possible to alert them of the situation of your stolen credit card details. Ask to replace your card with different security code, making the stolen card details invalid for use. ";
	private final String GEO_DESC = "From a physical security standpoint your location can provide the location of your home, your current location or location of others. Be careful using location features as not only can it potientially put you at risk but others. ";
	private final String HIST_PASS_DESC = "Historical passwords provide an insight to how you make passwords, in which case if similiar, try to use a new password that is completely different to your previous entries. ";
	private final String IM_ID_DESC = "Contact your Instant Messaging provider and tell them to not allow any password changes over the phone, or at the very least have a new verbal password repeated before reseting passwords over the phone. ";
	private final String ADDR_DESC = "Addresses can be used to verify an identity of a user in combination with identification. The implications of this being in combination with other data this can be used to steal an identity or even redirect mail using the postal service. ";
	private final String PHONE_DESC = "Stolen phone numbers can be used to direct spam or send harrassing phone calls. ";
	private final String GOV_DESC = "Government Ids are the easiest way for an identity to be stolen. Contact your local id issuer to report this incident and if possible make the old Id invalid by requesting a new id. ";

	// after loop
	private final String SPECIAL_PATTERNS_DESC = "Although not directly a risk, data such as web or shopping activity can be analysed to determine predictable behaviour, as this is what businesses use to increase product and service quality. To extend this concept it is possible to track and estimate users travel plans based on patterns of location based data in social networks. ";
	private final String FINAL_MSG = "Even though a single piece of breached information may not provide a sizable risk the main issue is the combination of data. Try to understand the implications of publically sharing data on social networks for example. The incremental combination of data exposure can damage the privacy and safety of you but others around you. \n\n";

	// credit
	private final String WATERMARK = "**GENERATED USING PROTECT MY PRIVACY FOR ANDROID**\n\n";
	private final String CREDIT = "Developed by Matthew Smithies\nEmail: info@mattsmithies.co.uk\nWeb: www.mattsmithies.co.uk";
	private String userStrategy; // generated strategy of breach

	public UserStrategy(List<String> issues, String domain) {
		this.userStrategy = generateBreachReport(issues, domain);
	}

	private String generateBreachReport(List<String> issues, String domain) {
		StringBuilder report = new StringBuilder();
		boolean patternWarning = true;

		report.append(addWaterMark());

		report.append(addDomainAddress(domain));

		for (String issue : issues) {
			if (issue.contains(EMAIL_ADDRESSES))
				report.append(EMAIL_DESC);

			if (issue.contains(PASSWORD_HINTS))
				report.append(HINTS_DEC);

			if (issue.contains(PASSWORDS))
				report.append(PASSWORDS_DESC);

			if (issue.contains(USERNAMES))
				report.append(USERNAME_DESC);

			if (issue.contains(CREDIT_CARDS))
				report.append(CREDIT_DESC);

			if (issue.contains(GEO))
				report.append(GEO_DESC);

			if (issue.contains(HIST_PASS))
				report.append(HIST_PASS_DESC);

			if (issue.contains(IM_IDS))
				report.append(IM_ID_DESC);

			if (issue.contains(ADDRS))
				report.append(ADDR_DESC);

			if (issue.contains(PHONE_NOS))
				report.append(PHONE_DESC);

			if (issue.contains(GOVERNMENT_IDS))
				report.append(GOV_DESC);

			if (issue.contains(WEB_ACTIVITY) || issue.contains(REWARD_BALANCES)
					|| issue.contains(PURCHASES)) {
				if (patternWarning) {
					report.append(SPECIAL_PATTERNS_DESC);
					patternWarning = false;
				}
			}
		}

		report.append(FINAL_MSG);

		report.append(addWaterMark());
		report.append(addCredit());

		return report.toString();
	}

	private String addDomainAddress(String domain) {
		return DOMAIN_EMAIL + domain + DOMAIN_STRING + "\n\n";
	}

	private String addWaterMark() {
		return ProtectApplication.WATER_MARK_EMAIL ? WATERMARK : "";
	}

	private String addCredit() {
		return CREDIT;
	}

	public String getStrategy() {
		return userStrategy;
	}

}
