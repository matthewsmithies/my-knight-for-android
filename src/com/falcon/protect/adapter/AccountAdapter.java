package com.falcon.protect.adapter;

import java.util.List;

import com.falcon.font.AppFont;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.ResponceStatus;
import com.falcon.myknight.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AccountAdapter extends ArrayAdapter<Account> {
	
	private static final String MY_ACCOUNTS = "My Accounts";
	private static final String MY_STATS = "My Stats";
//	private static final String MY_ACHEIVEMENTS = "My Acheivements";
	private static final String CREDITS = "Credits";
	
	public static final String[] PAGES = {MY_ACCOUNTS, MY_STATS, CREDITS};

//	public static final String[] PAGES = {MY_ACCOUNTS, MY_STATS, MY_ACHEIVEMENTS, CREDITS};


	public AccountAdapter(Context context, List<Account> accounts) {
		super(context, -1, accounts);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.account_grid_unit, null);
		}

		Account account = getItem(position);
		String[] email = account.getEmail().split("@");

		TextView title = (TextView) convertView.findViewById(R.id.EmailTitle);
		TextView suffix = (TextView) convertView.findViewById(R.id.EmailSuffix);
		TextView status = (TextView) convertView.findViewById(R.id.EmailStatus);

		AppFont appFont = new AppFont(getContext());
		title.setTypeface(appFont.getBoldFont());
		suffix.setTypeface(appFont.getBasicFont());
		status.setTypeface(appFont.getBasicFont());

		RelativeLayout layout = (RelativeLayout) convertView
				.findViewById(R.id.GridCell);

		setLayoutBorder(layout, account, status);

		title.setText(email[0]);
		suffix.setText("@" + email[1]);

		return convertView;
	}

	private void setLayoutBorder(RelativeLayout layout, Account account,
			TextView status) {
		switch (account.getResponceStatus().getStatusCode()) {
		case ResponceStatus.LOADING:
			layout.setBackgroundResource(R.drawable.grid_view_loading);
			status.setText("Currently querying account");
			break;

		case ResponceStatus.EMAIL_PRESENT:
			layout.setBackgroundResource(R.drawable.grid_view_problem);
			status.setText(issueBuilder(account));
			break;

		case ResponceStatus.NOT_FOUND:
			layout.setBackgroundResource(R.drawable.grid_view_ok);
			status.setText("Your account does not appear to be breached");
			break;

		default:
			break;
		}
	}

	private String issueBuilder(Account account) {

		if (account.getNoBreaches() > 1) {
			return "There are " + account.getNoBreaches()
					+ " data breaches linked with this account";
		} else {
			return "There is " + account.getNoBreaches()
					+ " data breach linked with this account";
		}

	}
}
