package com.falcon.protect.adapter;

import java.util.List;

import com.falcon.font.AppFont;
import com.falcon.protect.activities.IssueActivity;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.ProtectApplication;
import com.falcon.protect.dialog.ConfirmFix;
import com.falcon.protect.dialog.ShowDescription;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.Breach;
import com.falcon.protect.model.User;
import com.falcon.myknight.R;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BreachesAdapter extends ArrayAdapter<Breach> {

	private ProtectActivity protectActivity;
	private int accountPosition;
	
	public BreachesAdapter(ProtectActivity activity, List<Breach> breaches, int accountPosition) {
		super(activity.getContext(), -1, breaches);
		this.protectActivity = activity;
		this.accountPosition = accountPosition;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.account_breach_unit, null);
		}

		Breach breach = getItem(position);

		TextView title = (TextView) convertView.findViewById(R.id.BreachTitle);
		Button issue = (Button) convertView.findViewById(R.id.accountDiagnose);
		Button fix = (Button) convertView.findViewById(R.id.accountFix);

		AppFont appFont = new AppFont(getContext());
		title.setTypeface(appFont.getBoldFont());
		issue.setTypeface(appFont.getBasicFont());
		fix.setTypeface(appFont.getBasicFont());

		issue.setOnTouchListener(buttonTouch(issue));
		fix.setOnTouchListener(buttonTouch(fix));

		title.setText(breach.getTitle());

		backgroundClick(convertView, position);
		fix.setOnClickListener(confirmDialog(breach, position));
		issue.setOnClickListener(issueClick(breach));
		
		initialiseCellBorder(convertView, position);

		return convertView;
	}

	private void initialiseCellBorder(View view, int position) {
		RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.AccountCell);
		User user = new User(getContext());
		Account account = user.getAccounts().get(getAccountPosition());
		
		if(account.getBreachAtPos(position))
			layout.setBackgroundResource(R.drawable.grid_view_ok);
	}

	private OnClickListener issueClick(final Breach breach) {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent issueIntent = new Intent(getContext(),
						IssueActivity.class);
				issueIntent.putExtra(Breach.BREACH, breach.getRaw().toString());
				getContext().startActivity(issueIntent);
			}
		};
	}

	private OnClickListener confirmDialog(final Breach breach, final int position) {
		return new OnClickListener() {
			@Override
			public void onClick(View v) {
				new ConfirmFix(getProtectActivity(), breach, position, getAccountPosition()).show();
			}
		};
	}

	private void backgroundClick(View convertView, final int position) {
		RelativeLayout relativeLayout = (RelativeLayout) convertView
				.findViewById(R.id.AccountCell);

		relativeLayout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new ShowDescription(getContext(), getItem(position)).show();
			}
		});
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}
	

	private int getAccountPosition() {
		return accountPosition;
	}
	
	public ProtectActivity getProtectActivity() {
		return protectActivity;
	}

}
