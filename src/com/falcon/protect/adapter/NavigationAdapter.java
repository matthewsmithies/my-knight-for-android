package com.falcon.protect.adapter;

import java.util.List;

import com.falcon.font.AppFont;
import com.falcon.myknight.R;
import com.falcon.protect.activities.CreditsActivity;
import com.falcon.protect.activities.ProtectActivity;
import com.falcon.protect.activities.StatsActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class NavigationAdapter extends ArrayAdapter<String> implements
		OnItemClickListener {

	public static final byte MAIN = 0;
	public static final byte STATS = 1;
//	public static final byte ACHIEVE = 2;
	public static final byte CREDITS = 2;
	
	public static final String MY_MAIN = "My Account";
	public static final String MY_STATS = "My Stats";
	public static final String ACHIEVEMENTS = "My Acheivements";
	public static final String MY_CREDITS = "Credits";
	
	public static final String[] PAGES = {MY_MAIN, MY_STATS, MY_CREDITS};

	public static final String SELECTED = "Selected";

	private ListView drawerList;
	private DrawerLayout drawerLayout;
	private ProtectActivity protectActivity;

	private byte currentState; // current activity in use

	public NavigationAdapter(byte currentView, ListView drawerList,
			DrawerLayout drawerLayout, ProtectActivity activity,
			List<String> objects) {
		super(activity.getContext(), -1, objects);
		this.currentState = currentView;
		this.drawerLayout = drawerLayout;
		this.drawerList = drawerList;
		this.protectActivity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null)
			convertView = View.inflate(getContext(),
					R.layout.chapter_list_item, null);

		String content = getItem(position);

		AppFont appFont = new AppFont(getContext());
		TextView element = (TextView) convertView
				.findViewById(R.id.textElement);
		ImageView icon = (ImageView) convertView
				.findViewById(R.id.navigationImage);

		element.setTypeface(appFont.getBoldFont());
		element.setText(content);

		icon.setImageResource(getIcon(position));

		drawerList.setOnItemClickListener(this);

		return convertView;
	}

	private int getIcon(int position) {
		switch (position) {
		case 0:
			return R.drawable.ic_action_planet;

		case 1:
			return R.drawable.ic_action_line_chart;

		case 2:
			return R.drawable.ic_action_android;
			//return R.drawable.ic_action_achievement;

		default:
			return R.drawable.ic_action_android;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		getDrawerLayout().closeDrawers();

		if (position == currentState) return;
		if (currentState != MAIN) getProtectActivity().finish();
		
		getDrawerList().setSelection(MAIN);
		
		switch (position) {
		case MAIN:
			getProtectActivity().finish();
			break;

		case STATS:
			startActivity(StatsActivity.class, getContext());
			break;

//		case ACHIEVE:
//
//			break;

		case CREDITS:
			startActivity(CreditsActivity.class, getContext());
			break;

		default:
			break;
		}
	}

	private void startActivity(Class<?> cls, Context context) {
		Intent intent = new Intent(context, cls);
		context.startActivity(intent);
	}

	public ListView getDrawerList() {
		return drawerList;
	}

	public DrawerLayout getDrawerLayout() {
		return drawerLayout;
	}

	public byte getCurrentView() {
		return currentState;
	}

	public ProtectActivity getProtectActivity() {
		return protectActivity;
	}

}
