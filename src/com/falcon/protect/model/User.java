package com.falcon.protect.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.falcon.io.ReadUser;
import com.falcon.io.WriteUser;

import android.content.Context;

/*************************************
 * Note - RULES: An user can have many accounts: An account must have 1
 * response; An account must have 1 email. An account may have many breaches;
 **************************************/
public class User {

	public static final String ACCOUNTS = "accounts";

	private ArrayList<Account> userAccounts = new ArrayList<Account>();
	private ReadUser readUser;

	private Context context;

	// private String name;

	// private UserInformation userInformation
	// private StickerPack userStickers!
	// private Settings userSettings;
	// private Social userSocial;

	// public User(JSONObject pwnObject, String email) {
	// // TODO Auto-generated constructor stub
	// }

	// future String extractData();
	// future void Social...();

	// currently only list of accounts

	// get user file if any.
	public User(Context context) {
		this.context = context;
		this.readUser = new ReadUser(context);

		if (getReadUser().getIfExists()) {
			this.userAccounts = importUserdata(readUser);
		}
	}

	// deserialise user file
	private ArrayList<Account> importUserdata(ReadUser readUser) {
		ArrayList<Account> importAccounts = new ArrayList<Account>();

		JSONArray accountsJson = readUser.extractAccounts();
		// Log.e("json", accountsJson.toString());

		for (int i = 0; i < accountsJson.length(); i++) {
			JSONObject object = readUser.getJsonObject(accountsJson, i);
			importAccounts.add(new Account(object));
		}

		return importAccounts;
	}

	public boolean exportUserData() {
//		Log.i("generate", generateUserData());
		WriteUser writeUser = new WriteUser(generateUserData(), getContext());
		return writeUser.getWriteSuccess();
	}

	// serialise
	public String generateUserData() {
		JSONObject user = new JSONObject();
		JSONArray accounts = new JSONArray();

		for (Account account : getAccounts()) {
			accounts.put(account.exportJsonAccounts());
		}

		try {
			user.put(ACCOUNTS, accounts);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return user.toString();
	}

	private Context getContext() {
		return context;
	}

	public void removeAccount(String email) {

		for (int i = 0; i < getAccounts().size(); i++) {
			Account account = getAccounts().get(i);

			if (account.getEmail() == email) {
				getAccounts().remove(i);
				break;
			}
		}
	}

	public int getAccountsSize() {
		return getAccounts().size();
	}

	public int generateRating() {
		if (getTotalBreaches() == 0)
			return 100;

		float breaches = (float) getFixedBreaches()
				/ (float) getTotalBreaches() * 100;

		return (int) Math.round(breaches);
	}

	public int getFixedBreaches() {
		int count = 0;
		for (Account account : getAccounts()) {
			for (int i = 0; i < account.getAccountStatus().length(); i++) {
				if (account.getBreachAtPos(i))
					count++;
			}
		}
		return count;
	}

	public int getTotalBreaches() {
		int count = 0;
		for (Account account : getAccounts()) {
			count += account.getAccountStatus().length();
		}
		return count;
	}

	public int getBreachedCount() {
		return countAccounts(ResponceStatus.EMAIL_PRESENT);
	}

	public int getNotBreachedCount() {
		return countAccounts(ResponceStatus.NOT_FOUND);
	}

	private int countAccounts(int ResponceType) {
		int count = 0;
		for (Account account : getAccounts()) {
			if (account.getResponceStatus().getStatusCode() == ResponceType)
				count++;
		}
		return count;
	}

	public void addAccount(Account account) {
		getAccounts().add(0, account);
	}

	public ArrayList<Account> getAccounts() {
		return userAccounts;
	}

	public ReadUser getReadUser() {
		return readUser;
	}

	// probably not needed
	public void setAccounts(ArrayList<Account> accounts) {
		this.userAccounts = accounts;
	}

}
