package com.falcon.protect.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/*
 {
 "Name":"Adobe",
 "Title":"Adobe",
 "Domain":"adobe.com",
 "BreachDate":"2013-10-04",
 "AddedDate":"2013-12-04T00:00Z",
 "PwnCount":152445165,
 "Description":"The big one. In October 2013, 153 million accounts were breached with each containing an internal ID, username, email, <em>encrypted</em> password and a password hint in plain text. The password cryptography was poorly done and <a href=\"http://stricture-group.com/files/adobe-top100.txt\"> many were quickly resolved back to plain text</a>. The unencrypted hints also <a href=\"http://www.troyhunt.com/2013/11/adobe-credentials-and-serious.html\">disclosed much about the passwords</a> adding further to the risk that hundreds of millions of Adobe customers already faced.",
 "DataClasses":["Email","Password","Password hint","Username"]
 },
 *///An Email has many breaches.
public class Breach {

	private String name;
	private String title;
	private String domain;
	private String breachDate;
	private String addedDate;
	private String pwnCount;
	private String description;
	private JSONArray dataClasses;
	private JSONObject raw;
	private boolean fixed = false;

	public static final String NAME = "Name";
	public static final String TITLE = "Title";
	public static final String DOMAIN = "Domain";
	public static final String BREACH = "BreachDate";
	public static final String ADDED = "AddedDate";
	public static final String PWN_COUNT = "PwnCount";
	public static final String DESCRIPTION = "Description";
	public static final String DATA_CLASSES = "DataClasses";
	public static final String IS_FIXED = "Fixed";

	public Breach(JSONObject pwnObject) {
		
		
		this.raw = pwnObject;

		try {
			this.name = pwnObject.getString(NAME);
			this.title = pwnObject.getString(TITLE);
			this.domain = pwnObject.getString(DOMAIN);
			this.breachDate = pwnObject.getString(BREACH);
			this.addedDate = pwnObject.getString(ADDED);
			this.pwnCount = pwnObject.getString(PWN_COUNT);
			this.description = pwnObject.getString(DESCRIPTION);
			this.dataClasses = pwnObject.getJSONArray(DATA_CLASSES);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public String getDomain() {
		return domain;
	}

	public String getBreachDate() {
		return breachDate;
	}

	public String getAddedDate() {
		return addedDate;
	}

	public String getPwnCount() {
		return pwnCount;
	}

	public String getDescription() {
		return description;
	}

	public JSONArray getDataClasses() {
		return dataClasses;
	}

	public void setFixed() {
		fixed = true;
	}

	public boolean isFixed() {
		return fixed;
	}

	public JSONObject getRaw() {
		return raw;
	}

}
