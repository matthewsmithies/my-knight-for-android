package com.falcon.protect.model;


public class ResponceStatus {

	//Pre-state before receiving response
	public static final int LOADING = 0;
	
	// 200 Ok — everything worked and there's a string array of pwned sites for
	// the account
	public static final int EMAIL_PRESENT = 200;

	// 400 Bad request — the account does not comply with an acceptable format
	// (i.e. it's an empty string)
	public static final int BAD_REQUEST = 400;

	// 403 Forbidden — no user agent has been specified in the request
	public static final int FORRBIDDEN = 403;

	// 403 Forbidden — no user agent has been specified in the request 404 Not
	// found — the account could not be found and has therefore not been pwne
	public static final int NOT_FOUND = 404;

	private int statusCode;
	
	//maybe user message?
	private String status;

	public ResponceStatus(int status) {
		this.statusCode = status;
	}

	public int getStatusCode() {
		return statusCode;
	}
	
	public String getStatus() {
		return status;
	}

	public void setLoading() {
		this.statusCode = LOADING;
	}

}
