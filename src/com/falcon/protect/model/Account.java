package com.falcon.protect.model;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

//The status and breaches of a single account
public class Account {

	public static final String EMAIL = "email";
	public static final String STATUS = "status";
	public static final String BREACHES = "breaches";
	public static final String FIXED = "fixed";
	public static final String ACC_POS = "AccountPositioAn";

	// status of each account
	private String accountStatus = "";

	// use arraylists to add objects from array.
	private ArrayList<Breach> accountBreaches = new ArrayList<Breach>();

	// response of getting the account
	private ResponceStatus responceStatus = null;

	private String email;
	private JSONArray rawBreaches = new JSONArray();

	// pre response account
	public Account(String email) {
		this.email = email;
		this.responceStatus = new ResponceStatus(ResponceStatus.LOADING);
	}

	// original server response
	public Account(HttpResponse httpResponse) {

		this.responceStatus = new ResponceStatus(httpResponse.getStatusLine()
				.getStatusCode());

		if (getResponceStatus().getStatusCode() == ResponceStatus.EMAIL_PRESENT) {
			JSONArray breachArray = convertResponse(httpResponse);
			initAccountStatus(breachArray.length());
			this.rawBreaches = breachArray;
			this.accountBreaches = extractBreaches(breachArray);
		}
	}

	// retrieve from file.
	public Account(JSONObject jsonObject) {
		try {
			JSONArray breaches = jsonObject.getJSONArray(BREACHES);
			this.accountStatus = jsonObject.getString(FIXED);
			this.email = jsonObject.getString(EMAIL);
			this.accountBreaches = extractBreaches(breaches);
			this.rawBreaches = breaches;
			this.responceStatus = new ResponceStatus(jsonObject.getInt(STATUS));
		} catch (JSONException e) {
			e.printStackTrace();
		}

//		Log.e("json status", getAccountStatus());
	}

	private ArrayList<Breach> extractBreaches(JSONArray array) {
		ArrayList<Breach> breaches = new ArrayList<Breach>();

		for (int i = 0; i < array.length(); i++) {
			try {
				JSONObject json = array.getJSONObject(i);
				breaches.add(new Breach(json));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return breaches;
	}

	private JSONArray convertResponse(HttpResponse httpResponse) {
		try {
			String raw = EntityUtils.toString(httpResponse.getEntity());
			return new JSONArray(raw);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.wtf("Account API retrival", "httpResponse is invalid.");
		return null;
	}

	// used to export to file
	public String exportAccountData() {
		return exportJsonAccounts().toString();
	}

	public JSONObject exportJsonAccounts() {

		try {
			JSONObject account = new JSONObject();
			account.put(BREACHES, getRawJson());
			account.put(STATUS, getResponceStatus().getStatusCode());
			account.put(EMAIL, getEmail());
			account.put(FIXED, getAccountStatus());
			return account;

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<Breach> getAccountBreaches() {
		return accountBreaches;
	}

	public int getNoBreaches() {
		return getAccountBreaches().size();
	}

	public ResponceStatus getResponceStatus() {
		return responceStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private JSONArray getRawJson() {
		return rawBreaches;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	// jI'm going to hell...
	public boolean getBreachAtPos(int pos) {
		return getAccountStatus().charAt(pos) == "1".charAt(0);
	}

	public void setBreachStatus(int position) {
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < getAccountStatus().length(); i++) {
			if (i == position) {
				builder.append(1);
			} else {
				builder.append(getAccountStatus().charAt(i));
			}
		}
		this.accountStatus = builder.toString();
	}

	public int getRemainingBreaches() {
		int remaining = 0;
		String status = getAccountStatus();

		for (int i = 0; i < status.length(); i++) {
			if (!getBreachAtPos(i)){
				remaining++;
			}
		}
		return remaining;
	}

	private void initAccountStatus(int size) {
		StringBuilder init = new StringBuilder();
		for (int i = 0; i < size; i++) {
			init.append(0);
		}

		accountStatus = init.toString();
	}
}
