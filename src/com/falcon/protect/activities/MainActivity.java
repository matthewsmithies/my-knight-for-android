package com.falcon.protect.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.falcon.font.AppFont;
import com.falcon.protect.adapter.NavigationAdapter;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.User;
import com.falcon.myknight.R;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

public class MainActivity extends ProtectActivity {

	private ListView drawerList;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		this.font = new AppFont(getContext());

		setContentView(R.layout.main);

		setupSectionsDrawerList();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		setProgressBarIndeterminateVisibility(false);
		initialiseMain();
	}

	// add views in
	public void setupSectionsDrawerList() {
		this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		this.drawerList = (ListView) findViewById(R.id.left_drawer);
		
		NavigationAdapter adapter = new NavigationAdapter(
				NavigationAdapter.MAIN, drawerList, drawerLayout,
				getProtectActivity(), getLoading());

		this.drawerList.setAdapter(adapter);

		drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View drawerView) {
			};

			public void onDrawerOpened(View drawerView) {
			};
		};
		this.drawerLayout.setDrawerListener(drawerToggle);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	private void initialiseMain() {
		if (hasSavedData()) {
			loadUserData();
		} else {
			initialLayout();
		}
	}

	private void loadUserData() {
		User user = new User(getContext());

		List<Account> accounts = user.getAccounts();
		setGridAdapter(accounts);
	}

	@Override
	protected void onStart() {
		super.onStart();
		getActionBar().setTitle(ProtectApplication.getTitle(getActivity()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public ArrayList<String> getLoading() {
		return new ArrayList<String>(Arrays.asList(NavigationAdapter.PAGES));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (drawerToggle.onOptionsItemSelected(item)) {
			ProtectApplication.hepticKeyPress(getContext());
			return true;
		}

		switch (item.getItemId()) {
		case R.id.add:
			AddNewEmail().show();

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private ProtectActivity getProtectActivity() {
		return this;
	}

}
