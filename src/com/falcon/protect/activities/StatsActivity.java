package com.falcon.protect.activities;

import java.util.ArrayList;
import java.util.Arrays;

import com.falcon.font.AppFont;
import com.falcon.myknight.R;
import com.falcon.protect.adapter.AccountAdapter;
import com.falcon.protect.adapter.NavigationAdapter;
import com.falcon.protect.model.User;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class StatsActivity extends ProtectActivity {

	private ListView drawerList;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;

	// percentage
	public static final int GREEN = 100;
	public static final int YELLOW = 95;
	public static final int ORANGE = 75;
	public static final int RED = 50;

	private User user;

	/**
	 * You have added x accounts. X of your accounts are not listed as breached.
	 * You have X breaches Linked with X accounts You have marked x breaches as
	 * fixed
	 * 
	 * Your current rating x%
	 * 
	 * for example 4 accounts, 2 are good, 1 has 1 breach, 1 has 3 breaches
	 * 
	 * green 95% yellow 75% orange 50% Red < 50%
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.my_stats_view);
		this.user = new User(getContext());

		if (getUser().getAccountsSize() == 0) {
			setContentView(R.layout.stats_init);
			initialiseTitle();
		} else {
			initialiseFont();
		}
		
		setupSectionsDrawerList();
		initialiseActionBar();
	}

	private void initialiseTitle() {
		AppFont appFont = new AppFont(getContext());
		TextView title = (TextView) findViewById(R.id.statsInit);
		title.setTypeface(appFont.getBasicFont());
	}

	private void initialiseFont() {
		AppFont appFont = new AppFont(getContext());

		TextView title = (TextView) findViewById(R.id.statsRatingTitle);
		TextView rating = (TextView) findViewById(R.id.statsRating);
		TextView added = (TextView) findViewById(R.id.statsAddedAccounts);
		TextView noBreached = (TextView) findViewById(R.id.statsSafeAcc);
		TextView linkedBreach = (TextView) findViewById(R.id.statsBreachesLinked);
		TextView resolved = (TextView) findViewById(R.id.statsResolved);

		title.setTypeface(appFont.getBoldFont());
		rating.setTypeface(appFont.getBoldFont());
		added.setTypeface(appFont.getBoldFont());
		noBreached.setTypeface(appFont.getBoldFont());
		linkedBreach.setTypeface(appFont.getBoldFont());
		resolved.setTypeface(appFont.getBoldFont());

		rating.setText(calulateRating());
		added.setText(getAddedAccount());
		noBreached.setText(getNoBreachAccounts());
		linkedBreach.setText(getLinkedAccounts());
		resolved.setText(getResolvedBreaches());

		rating.setTextColor(getResources().getColor(getRatingColor()));
	}

	private int getRatingColor() {
		int rating = getUser().generateRating();

		if (rating < RED) {
			return android.R.color.holo_red_dark;
		}

		if (rating < ORANGE) {
			return android.R.color.holo_orange_dark;
		}

		if (rating < YELLOW) {
			return R.color.yellow;
		}

		return android.R.color.holo_green_dark;
	}

	// plurals people!
	private CharSequence getNoBreachAccounts() {
		int count = getUser().getNotBreachedCount();

		if (count == 1) {
			return count + " account has not been breached";
		} else {
			return count + " accounts have not been breached";
		}
	}

	private CharSequence getResolvedBreaches() {
		int count = getUser().getFixedBreaches();

		if (count == 1) {
			return count + " breach has been marked as resolved";
		} else {
			return count + " breaches have been marked as resolved";
		}
	}

	private CharSequence getLinkedAccounts() {
		int bCount = getUser().getBreachedCount();
		int total = getUser().getTotalBreaches();

		String accounts = bCount == 1 ? " account" : " accounts";
		String breaches = total == 1 ? " breach" : " breaches";

		return "you have " + bCount + breaches + " linked with " + total
				+ accounts;
	}

	private CharSequence getAddedAccount() {
		int size = getUser().getAccountsSize();
		String account = size == 1 ? " account" : " accounts";

		return "you have added " + size + account;
	}

	private CharSequence calulateRating() {
		return getUser().generateRating() + "%";
	}

	public void initialiseActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setTitle("My Stats");
	}

	public void setupSectionsDrawerList() {
		this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		this.drawerList = (ListView) findViewById(R.id.left_drawer);

		NavigationAdapter adapter = new NavigationAdapter(
				NavigationAdapter.STATS, drawerList, drawerLayout,
				getProtectActivity(), getLoading());

		this.drawerList.setAdapter(adapter);

		drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View drawerView) {
			};

			public void onDrawerOpened(View drawerView) {
			};
		};
		this.drawerLayout.setDrawerListener(drawerToggle);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	public ArrayList<String> getLoading() {
		return new ArrayList<String>(Arrays.asList(AccountAdapter.PAGES));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (drawerToggle.onOptionsItemSelected(item)) {
			ProtectApplication.hepticKeyPress(getContext());
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private ProtectActivity getProtectActivity() {
		return this;
	}

	public User getUser() {
		return user;
	}

}
