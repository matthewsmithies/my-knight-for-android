package com.falcon.protect.activities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.falcon.font.AppFont;
import com.falcon.protect.dialog.ExportReport;
import com.falcon.protect.dialog.ShowDescription;
import com.falcon.protect.lexical.UserStrategy;
import com.falcon.protect.model.Breach;
import com.falcon.myknight.R;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IssueActivity extends ProtectActivity {

	private Breach breach;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String bundle = getIntent().getExtras().getString(Breach.BREACH);
		this.breach = new Breach(ProtectApplication.jsonFromString(bundle));

		setContentView(R.layout.issue_layout);
			
		getActionBar().setTitle(getBreach().getName());
		getActionBar().setSubtitle("Issues");
		
		initialiseLayout();
	}

	private void initialiseLayout() {

		TextView mainTitle = (TextView) findViewById(R.id.issueTitle);
		TextView issueCount = (TextView) findViewById(R.id.issueAmount);
		TextView issueBlockTitle = (TextView) findViewById(R.id.issueBlockTitle);
		TextView strategyBlockTitle = (TextView) findViewById(R.id.strategyBlockTitle);
		Button description = (Button) findViewById(R.id.issueDescriptionButton);

		AppFont appFont = new AppFont(getContext());

		mainTitle.setTypeface(appFont.getBoldFont());
		issueCount.setTypeface(appFont.getBasicFont());
		issueBlockTitle.setTypeface(appFont.getBoldFont());
		description.setTypeface(appFont.getBasicFont());
		strategyBlockTitle.setTypeface(appFont.getBoldFont());

		description.setOnTouchListener(buttonTouch(description));
		description.setOnClickListener(descriptionClick());

		mainTitle.setText(getBreach().getTitle());
		issueCount.setText(getIssues());

		List<String> issues = setUpIssueHolder(appFont);
		generateStrategy(issues, appFont);
	}

	private void generateStrategy(List<String> issues, AppFont appFont) {
		UserStrategy userStrategy = new UserStrategy(issues, getBreach().getDomain());

		TextView strategy = (TextView) findViewById(R.id.strategyInfo);
		Button export = (Button) findViewById(R.id.exportButton);

		strategy.setTypeface(appFont.getBasicFont());
		export.setTypeface(appFont.getBasicFont());

		strategy.setText(userStrategy.getStrategy());

		export.setOnTouchListener(buttonTouch(export));
		export.setOnClickListener(exportReport(userStrategy));
	}

	private View.OnClickListener exportReport(final UserStrategy strategy) {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new ExportReport(getCurrentActivity(), strategy.getStrategy())
						.show();
			}
		};
	}

	private List<String> setUpIssueHolder(AppFont appFont) {
		LinearLayout issueLayout = (LinearLayout) findViewById(R.id.issueHolder);
		List<String> issues = getDataBreaches();

		attachHighlight(issueLayout, appFont);

		for (String issue : issues) {
			TextView textView = new TextView(getContext());
			textView.setTextAppearance(getContext(),
					android.R.style.TextAppearance_DeviceDefault_Large);
			textView.setGravity(Gravity.CENTER);
			textView.setText(issue);
			textView.setTypeface(appFont.getBasicFont());
			issueLayout.addView(textView);
		}

		attachHighlight(issueLayout, appFont);

		return issues;
	}

	private void attachHighlight(LinearLayout layout, AppFont appFont) {
		TextView textView = new TextView(getContext());
		textView.setTextAppearance(getContext(),
				android.R.style.TextAppearance_DeviceDefault_Large);
		textView.setGravity(Gravity.CENTER);
		textView.setTypeface(appFont.getBasicFont());
		textView.setText("---");
		layout.addView(textView);
	}

	private OnClickListener descriptionClick() {
		return new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new ShowDescription(getContext(), getBreach()).show();
			}
		};
	}

	private View.OnTouchListener buttonTouch(final Button button) {
		return new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ProtectApplication.hepticKeyPress(getContext());
					button.setBackgroundResource(R.drawable.button_border_pressed);
				} else {
					button.setBackgroundResource(R.drawable.button_border);
				}

				return false;
			}
		};
	}

	private ArrayList<String> getDataBreaches() {
		ArrayList<String> dataList = new ArrayList<String>();

		for (int i = 0; i < getRawDataBreach().length(); i++) {
			try {
				dataList.add(getRawDataBreach().getString(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return dataList;
	}

	private String getIssues() {
		return getBreach().getPwnCount() + " " + getString(R.string.issue_num);
	}

	private JSONArray getRawDataBreach() {
		return getBreach().getDataClasses();
	}

	private Breach getBreach() {
		return breach;
	}

	private ProtectActivity getCurrentActivity() {
		return this;
	}
}
