package com.falcon.protect.activities;

import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.falcon.font.AppFont;
import com.falcon.io.Io;
import com.falcon.protect.adapter.AccountAdapter;
import com.falcon.protect.dialog.AddAccount;
import com.falcon.protect.dialog.DeleteAccount;
import com.falcon.protect.dialog.NoBreach;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.ResponceStatus;
import com.falcon.protect.model.User;
import com.falcon.protect.service.ResponseJsonListener;
import com.falcon.myknight.R;

public class ProtectActivity extends Activity implements ResponseJsonListener {

	private static boolean elementPress = true; // disable multi click listeners
	private AccountAdapter accountAdapter = null; // connects grid view
	protected AppFont font; // Font base for the application

	@Override
	public void onTaskComplete(Account account) {
		setProgressBarIndeterminateVisibility(false);
		defaultActionBar();

		User user = new User(getContext());
		user.removeAccount(account.getEmail());
		user.addAccount(account);
		user.exportUserData();
		
		removeInitialVisibility();
		setGridAdapter(user.getAccounts());
	}

	private void removeInitialVisibility() {
		RelativeLayout initial = (RelativeLayout) findViewById(R.id.initialLayout);
		initial.setVisibility(View.GONE);
	}

	private void initialiseClickListeners(final GridView gridView,
			final List<Account> accounts) {

		gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {

				view.startAnimation(AnimationUtils.loadAnimation(getContext(),
						R.anim.left_animation));
				deleteAccount(accounts, position).show();
				elementPress = false;
				return false;
			}
		});

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {

				if (elementPress) {

					final Account account = accounts.get(position);
					switch (account.getResponceStatus().getStatusCode()) {
					case ResponceStatus.NOT_FOUND:
						refreshDialog(account, position).show();
						break;

					case ResponceStatus.EMAIL_PRESENT:
						Intent intent = new Intent(getContext(),
								AccountActivity.class);
						// intent.putExtra(Account.BREACHES,
						// account.exportAccountData());
						intent.putExtra(Account.ACC_POS, position);
						getContext().startActivity(intent);
						break;

					default:
						break;
					}
				}
			}

			private Dialog refreshDialog(Account account, int position) {
				ProtectApplication.hepticKeyPress(getContext());
				return new NoBreach(getProtectActivity(), account,
						getListener(), position);
			}
		});
	}

	public void delete(List<Account> accounts, int position) {

		if (getAccountAdapter().getCount() == 1) {
			getContext().deleteFile(Io.FILENAME);
//			setContentView(R.layout.main);
			initialLayout();
		} else {
			User user = new User(getContext());
			user.getAccounts().remove(position);
			user.exportUserData();
		}

		getAccountAdapter().remove(accounts.get(position));
	}

	public void initialLayout() {
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.initialLayout);
		TextView title = (TextView) findViewById(R.id.initialTitle);
		title.setTypeface(getFont().getBasicFont());
		title.setOnClickListener(titleClick());
		layout.setVisibility(View.VISIBLE);
//		title.setVisibility(View.VISIBLE);
//		title.setBackgroundResource(R.drawable.button_border);
	}

	private OnClickListener titleClick() {
		return new OnClickListener() {

			@Override
			public void onClick(View v) {
				AddNewEmail().show();
			}
		};
	}

	protected Dialog AddNewEmail() {
		ProtectApplication.hepticKeyPress(getContext());
		return new AddAccount(getProtectActivity(), getListener());
	}

	protected Dialog deleteAccount(final List<Account> accounts, final int id) {
		ProtectApplication.hepticKeyPress(getContext());
		return new DeleteAccount(getProtectActivity(), id, accounts);
	}

	public void setGridAdapter(List<Account> accounts) {
		GridView gridView = (GridView) findViewById(R.id.accountGrid);
		this.accountAdapter = new AccountAdapter(getContext(), accounts);
		gridView.setAdapter(getAccountAdapter());
		initialiseClickListeners(gridView, accounts);
	}

	private void defaultActionBar() {
		getActionBar().setTitle("My Accounts");
	}

//	public int setMainView() {
//		return hasSavedData() ? R.layout.main : R.layout.initial_main;
//	}

	public boolean hasSavedData() {
		User user = new User(getContext());
		Log.i("saved data", "Content is present.");
		return user.getReadUser().getIfExists();
	}

	public AccountAdapter getAccountAdapter() {
		return accountAdapter;
	}

	private ProtectActivity getProtectActivity() {
		return this;
	}

	public AppFont getFont() {
		return font;
	}

	public Context getContext() {
		return this;
	}

	public Activity getActivity() {
		return this;
	}

	public ResponseJsonListener getListener() {
		return this;
	}

	// ugly but works
	public static void setPressTrue() {
		elementPress = true;
	}

}
