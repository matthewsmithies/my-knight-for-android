package com.falcon.protect.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.falcon.myknight.R;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;

public class ProtectApplication extends Application {

	//settings user for vibrate feedback.
	public static final boolean SETTINGS_VIBRATE = true;
	
	public static final boolean WATER_MARK_EMAIL = false;

	public static final String EMAIL_ACTION = "com.falcon.protect.service.EmailService";
	public static final String ROOT_URL = "https://www.haveibeenpwned.com/api/v2/breachedaccount/";

	// heptic feedback on button clicks, multi sensory dev.
	private static final int VIBRATION_TIME = 20;

	public static void hepticKeyPress(Context context) {
		if (SETTINGS_VIBRATE) {
			Vibrator v = (Vibrator) context
					.getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(VIBRATION_TIME);
		}
	}

	public static String getTitle(Activity activity) {
		return activity.getString(R.string.my_account);
	}

	public static boolean isInterwebsOn(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
	
	public static JSONObject jsonFromString(String raw) {
		try {
			return new JSONObject(raw);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return new JSONObject();
	}

}
