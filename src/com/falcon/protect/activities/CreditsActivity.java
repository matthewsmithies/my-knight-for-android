package com.falcon.protect.activities;

import java.util.ArrayList;
import java.util.Arrays;

import com.falcon.font.AppFont;
import com.falcon.myknight.R;
import com.falcon.protect.adapter.NavigationAdapter;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class CreditsActivity extends ProtectActivity {

	private ListView drawerList;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.credits_view);

		setupSectionsDrawerList();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setTitle("Credits");

		setUpCredits();
	}

	public void setupSectionsDrawerList() {
		this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		this.drawerList = (ListView) findViewById(R.id.left_drawer);

		NavigationAdapter adapter = new NavigationAdapter(
				NavigationAdapter.CREDITS, drawerList, drawerLayout,
				getProtectActivity(), getLoading());

		this.drawerList.setAdapter(adapter);

		drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View drawerView) {
			};

			public void onDrawerOpened(View drawerView) {
			};
		};
		this.drawerLayout.setDrawerListener(drawerToggle);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	private void setUpCredits() {
		AppFont appFont = new AppFont(getContext());

		TextView mainTitle = (TextView) findViewById(R.id.creditsTitle);
		TextView devTitle = (TextView) findViewById(R.id.creditDev);
		TextView mattTitle = (TextView) findViewById(R.id.creditsMatt);
		TextView mattWeb = (TextView) findViewById(R.id.creditsMattWeb);
		TextView apiTitle = (TextView) findViewById(R.id.creditApiTitle);
		TextView troyTitle = (TextView) findViewById(R.id.creditPwned);
		TextView troyWeb = (TextView) findViewById(R.id.creditPwnedWeb);

		mainTitle.setTypeface(appFont.getBoldFont());
		devTitle.setTypeface(appFont.getBoldFont());
		mattTitle.setTypeface(appFont.getBasicFont());
		mattWeb.setTypeface(appFont.getBasicFont());
		apiTitle.setTypeface(appFont.getBoldFont());
		troyTitle.setTypeface(appFont.getBasicFont());
		troyWeb.setTypeface(appFont.getBasicFont());
		
		mattWeb.setOnClickListener(startWebActivity(mattWeb.getText().toString()));
		troyWeb.setOnClickListener(startWebActivity(troyWeb.getText().toString()));
	}
	
	private View.OnClickListener startWebActivity(final String url){
		return new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ProtectApplication.hepticKeyPress(getContext());
				Intent intent = new Intent(getContext(), WebViewActivity.class);
				intent.putExtra(WebViewActivity.URL_KEY, url);
				startActivity(intent);
			}
		};
	}

	public ArrayList<String> getLoading() {
		return new ArrayList<String>(Arrays.asList(NavigationAdapter.PAGES));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (drawerToggle.onOptionsItemSelected(item)) {
			ProtectApplication.hepticKeyPress(getContext());
			return true;
		}

		switch (item.getItemId()) {
		case R.id.add:
			AddNewEmail().show();

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private ProtectActivity getProtectActivity() {
		return this;
	}
}
