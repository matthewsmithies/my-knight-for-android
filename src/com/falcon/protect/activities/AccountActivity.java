package com.falcon.protect.activities;

import com.falcon.font.AppFont;
import com.falcon.protect.adapter.BreachesAdapter;
import com.falcon.protect.model.Account;
import com.falcon.protect.model.User;
import com.falcon.myknight.R;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

public class AccountActivity extends ProtectActivity {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	// /private NavigationDrawerFragment mNavigationDrawerFragment;
	private Account account;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}. 
	 */
	// private CharSequence mTitle;

	private final String MSG = " connected with this account.";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_account);
		Bundle bundle = getIntent().getExtras();
		// .getString(Account.BREACHES));
		int accountPosition = bundle.getInt(Account.ACC_POS);

		User user = new User(getContext());
		this.account = user.getAccounts().get(accountPosition);
		initialiseActionBar(getAccount());

		AppFont appFont = new AppFont(getContext());
		TextView title = (TextView) findViewById(R.id.accountInfo);
		initialiseTitle(title, accountPosition);
		title.setTypeface(appFont.getBoldFont());

		GridView gridView = (GridView) findViewById(R.id.accountActivityGrid);
		BreachesAdapter breachesAdapter = new BreachesAdapter(getProActivity(),
				getAccount().getAccountBreaches(), accountPosition);

		gridView.setAdapter(breachesAdapter);

	}
	
	private void initialiseActionBar(Account account) {
		getActionBar().setTitle(account.getEmail().split("@")[0]);
		getActionBar().setSubtitle("Breaches");
	}

	private void initialiseTitle(TextView title, int acc) {
		User user = new User(getContext());
		Account account = user.getAccounts().get(acc);
		int remaining = account.getRemainingBreaches();
		title.setText(getTitleMsg(remaining));
		title.setBackgroundResource(getBackgroundTitle(remaining));
	}

	private int getBackgroundTitle(int remaining) {
		return remaining == 0 ? R.drawable.grid_view_ok
				: R.drawable.grid_view_problem;
	}

	private String getTitleMsg(int remaining) {
		if (remaining == 0) {
			return "All of the breaches have been marked as fixed.";
		}

		String breaches = remaining > 1 ? " breaches" : " breach";
		return "You have " + remaining + breaches + MSG;
	}

	private ProtectActivity getProActivity() {
		return this;
	}

	private Account getAccount() {
		return account;
	}

}
