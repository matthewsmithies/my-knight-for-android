package com.falcon.protect.activities;

import com.falcon.myknight.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {

	public static final String URL_KEY = "url";

	private String url;
	private WebView webView;

	@SuppressLint("SetJavaScriptEnabled")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_activity);

		Bundle bundle = getIntent().getExtras();

		this.url = bundle.getString(URL_KEY);

		webView = (WebView) findViewById(R.id.webActivity);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new SSLTolerentWebViewClient());
		
		 webView.setWebChromeClient(new WebChromeClient() {
		 });
		webView.loadUrl("http://"+url);
	}

	@Override
	public void onBackPressed() {
		if (webView.canGoBack()) {
			webView.goBack();
		} else {
			super.onBackPressed();
		}
	}

	private class SSLTolerentWebViewClient extends WebViewClient {

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			handler.proceed();
		}

	}
}
