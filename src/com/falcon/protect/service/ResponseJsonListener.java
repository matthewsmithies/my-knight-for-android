package com.falcon.protect.service;

import com.falcon.protect.model.Account;

public interface ResponseJsonListener {
	void onTaskComplete(Account result);
}
