package com.falcon.font;

import android.content.Context;
import android.graphics.Typeface;

public class AppFont {

	private static final String TYPE = "Roboto-Thin.ttf";
	private static final String BOLD = "RobotoCondensed-Bold.ttf";
	
	private Typeface basicType;
	private Typeface boldType;
	private Context context;
	
	public AppFont(Context context) {
		this.context = context;
		this.basicType = initialiseType(TYPE);
		this.boldType = initialiseType(BOLD);
	}
	
	private Typeface initialiseType(String type) {
		return Typeface.createFromAsset(getContext().getAssets(), TYPE);
	}
	
	private Context getContext() {
		return context;
	}
	
	public Typeface getBoldFont() {
		return boldType;
	}
	
	public Typeface getBasicFont() {
		return basicType;
	}

}
