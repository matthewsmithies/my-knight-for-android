package com.falcon.io;

import android.content.Context;

public class WriteUser extends WriteXmlData {
	
	private boolean writeSuccess;
	
	public WriteUser(String data, Context context) {
		super(data, Io.FILENAME);
		this.writeSuccess = writeToXml(context);
	}
	
	public boolean getWriteSuccess() {
		return writeSuccess;
	}
}
