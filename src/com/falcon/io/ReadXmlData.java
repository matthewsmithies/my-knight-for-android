package com.falcon.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;

class ReadXmlData {

	private final Context context;
	private final Boolean fileExists;
	private final String filename;

	protected ReadXmlData(Context context, String filename) {
		this.context = context;
		this.fileExists = CheckFileExists(filename);
		this.filename = filename;
	}

	private Boolean CheckFileExists(String filename) {
		File file = context.getFileStreamPath(filename);
		if (file.exists())
			return true;
		return false;
	}

	protected String retriveXmlData() {

		String xmlFile = this.filename;
		ArrayList<String> readData = new ArrayList<String>();

		try {
			FileInputStream fis = context.openFileInput(xmlFile);
			InputStreamReader isr = new InputStreamReader(fis);
			char[] inputBuffer = new char[fis.available()];
			isr.read(inputBuffer);
			String data = new String(inputBuffer);
			isr.close();
			fis.close();

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new StringReader(data));

			int eventType = 0;
			eventType = xpp.getEventType();

			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_DOCUMENT) {
					System.out.println("Start document");
				} else if (eventType == XmlPullParser.START_TAG) {
					System.out.println("Start tag " + xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					System.out.println("End tag " + xpp.getName());
				} else if (eventType == XmlPullParser.TEXT) {
					readData.add(xpp.getText());
				}

				eventType = xpp.next();
			}
			
			return readData.get(0);

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected JSONObject retriveXmlJsonData() {
		try {
			return new JSONObject(retriveXmlData());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Boolean ifFileExists() {
		return fileExists;
	}

}
