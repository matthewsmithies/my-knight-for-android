package com.falcon.io;

import android.app.Activity;
import android.content.Context;

import com.falcon.protect.model.User;

public class DeleteAccount {

	public DeleteAccount(Activity activity, Context context, String email) {
		User user = new User(context);
		
		if(user.getAccounts().size() == 1) {
			context.deleteFile(Io.FILENAME);
		} else {
			user.removeAccount(email);
			user.exportUserData();
		}
	}
	
}
