package com.falcon.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.util.Xml;

class WriteXmlData {

	private String data;

	private final String HEAD_NODE = Io.BRAND_HEAD;
	private final String NODE = Io.BRAND_DATA;
	
	private final String filename;

	public WriteXmlData(String data, String filename) {
		this.data = data;
		this.filename = filename;
	}

	public boolean writeToXml(Context context) {

		try {
			FileOutputStream fileOutputStream = context.openFileOutput(
					this.filename, Context.MODE_PRIVATE);

			XmlSerializer xmlSerializer = Xml.newSerializer();
			StringWriter stringWriter = new StringWriter();

			xmlSerializer.setOutput(stringWriter);
			xmlSerializer.startDocument("UTF-8", true);
			xmlSerializer.startTag(null, HEAD_NODE);

			xmlSerializer.startTag(null, NODE);
			xmlSerializer.text(data);
			xmlSerializer.endTag(null, NODE);

			xmlSerializer.endTag(null, HEAD_NODE);
			xmlSerializer.endDocument();
			xmlSerializer.flush();

			String dataWrite = stringWriter.toString();
			fileOutputStream.write(dataWrite.getBytes());
			fileOutputStream.close();

			return true;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public String getFilename() {
		return filename;
	}

}
