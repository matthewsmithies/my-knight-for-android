package com.falcon.io;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.falcon.protect.model.User;

import android.content.Context;

public class ReadUser extends ReadXmlData {

	private JSONObject jsonData;
	private String rawData;
	private Boolean ifExists;

	public ReadUser(Context context) {
		super(context, Io.FILENAME);
		this.ifExists = ifFileExists();

		if (getIfExists()) {
			this.jsonData = retriveXmlJsonData();
			this.rawData = retriveXmlData();
		}
	}

	public JSONObject getJsonObject(JSONArray array, int iter) {
		try {
			return array.getJSONObject(iter);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new JSONObject();
	}

	public JSONArray extractAccounts() {
		try {
			return getJsonData().getJSONArray(User.ACCOUNTS);
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public JSONObject getJsonData() throws NullPointerException {
		return jsonData;
	}

	public String getRawData() throws NullPointerException {
		return rawData;
	}

	public Boolean getIfExists() {
		return ifExists;
	}
}
