package com.falcon.intent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class Email {

	private static final String SUBJECT = "Protect my privacy";

	private String primaryEmail;
	private String content;

	public Email(String primaryEmail, String content) {
		this.primaryEmail = primaryEmail;
		this.content = content;
	}

	public void sendEmail(Activity activity) {
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.setType("message/rfc822");
		intent.setData(Uri.parse("mailto:"+getPrimaryEmail())); 
		intent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT);
		intent.putExtra(Intent.EXTRA_TEXT, getContent());
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		activity.startActivity(intent);
	}

	public String getPrimaryEmail() {
		return primaryEmail;
	}
	
	public String getContent() {
		return content;
	}

}
