package com.falcon.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.widget.EditText;

/**
 * 
 * Helper class for user input, for login, sign up and recording aspects of the
 * application.
 * 
 * @author Matthew
 * 
 */
public class RegexComponent {

	private static final String EMAIL_REGEX = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

	/**
	 * This helps to check that the email is valid, through a regex before the
	 * email is sent forward to the server.
	 * 
	 * @param email
	 * @return boolean
	 */
	public static boolean isEmailValid(EditText email) {
		
		CharSequence inputStr = email.getText().toString();

		Pattern pattern = Pattern
				.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		return matcher.matches();
	}

}
